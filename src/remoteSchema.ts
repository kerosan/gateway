import { ApolloLink } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { HttpLink } from 'apollo-link-http'
import { introspectSchema, makeRemoteExecutableSchema } from 'graphql-tools'
import https from 'https'
import fetch from 'cross-fetch'

export const getSchema = async ({ uri, name }: any) => {
  const link = ApolloLink.from([
    setContext((context, previousContext) => {
      const Authorization = previousContext?.graphqlContext?.authorization
      return {
        headers: Authorization
          ? {
              Authorization,
            }
          : {},
      }
    }),
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[${Date.now()}][GraphQL error][${name}]: Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        )

      if (networkError) {
        console.log(`[${Date.now()}][Network error][${name}]: ${networkError}`)
      }
    }),
    new HttpLink({
      uri,
      fetch,
      fetchOptions: {
        agent: new https.Agent({
          rejectUnauthorized: false,
        }),
      },
    }),
  ])
  const schema = await introspectSchema(link)
  return makeRemoteExecutableSchema({
    schema,
    link,
  })
}
