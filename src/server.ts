import { ApolloServer } from 'apollo-server'

import { getSchema } from './remoteSchema'

const uri = process.env.API_URL || 'https://app.jenery.com:9876/graphql'

getSchema({ uri, name: 'app' }).then(schema => {
  const server: any = new ApolloServer({
    schema,
    engine: { sendHeaders: { all: true } },
    context: ctx => {
      return { authorization: ctx.req.header('authorization') }
    },
  })

  server.listen().then(({ url }: any) => {
    console.log(`🚀 Server ready at ${url}`)
  })
})
